# use node 16 alpine image
FROM node:16-alpine

# create work directory in app folder
WORKDIR /nuxtapp

# install required packages for node image
RUN apk --no-cache add openssh g++ make python3 git

# copy over package.json files
COPY package.json /nuxtapp/
COPY yarn.lock /nuxtapp/

# install all depencies
RUN yarn install

# copy over all files to the work directory
ADD . /nuxtapp

# build the project
RUN yarn build

# expose the host and port 3000 to the server
ENV HOST 0.0.0.0
ENV PORT 3000
ENV NODE_ENV production

EXPOSE 3000
ENV PATH /app/node_modules/.bin:$PATH
# run the build project with node
ENTRYPOINT ["node", ".output/server/index.mjs"]
