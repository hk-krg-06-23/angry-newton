import { defineStore } from 'pinia';
import { ref } from 'vue'

export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    token: JSON.parse(localStorage.getItem('token')),
    returnUrl: '/cabinet'
  }),
  actions: {
    async login(phone, code) {
      
      const res = await $fetch('auth/confirm-code', {
        method: 'POST',
        baseURL: 'https://api.ishotel.xyz',
        body: {
            "phone": phone,
            "code": code
        }
    })
    
    this.token = res.token,
    localStorage.setItem('token', JSON.stringify(res.token));
    },

    logout() {
      this.token = null;
      localStorage.removeItem('token');
      
  }
  }
});